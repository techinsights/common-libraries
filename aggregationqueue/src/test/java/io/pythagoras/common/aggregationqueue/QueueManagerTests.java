package io.pythagoras.common.aggregationqueue;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * TODO: Figure out how to make this testable.  Dependency on Postgres makes it prone to break depending modules test cycles.
 */

@ExtendWith(SpringExtension.class)
@ActiveProfiles("local")
@Disabled
public class QueueManagerTests {

    @Autowired
    QueueManager queueManager;

    /**
     * Insert single job.  No conflicts.
     */
    @Test
    public void insertJob1() {
        Task job = queueManager.insertTask("REPORT~1234", "REPORT");
        Assertions.assertTrue(job != null);
    }

    /**
     * Insert single job.  Conflict with existing job not started.
     */
    @Test
    public void insertJob2() {
        Task job1 = queueManager.insertTask("REPORT~1234", "REPORT");
        Task job2 = queueManager.insertTask("REPORT~1234", "REPORT");
        Assertions.assertTrue(job1 != null);
    }

    /**
     * Insert single job.  1 other job not started already exists.
     */
    @Test
    public void insertJob3() {

    }


    @Test
    public void claimNextJob1() {
        Task job1 = queueManager.insertTask("REPORT~1234", "REPORT");
        Task job2 = queueManager.insertTask("REPORT~432", "REPORT");
        Task job3 = queueManager.insertTask("REPORT~5678", "REPORT");
        Task jobResult1 = queueManager.claimNextTask();
        Assertions.assertTrue(jobResult1 != null);
        Task jobResult2 = queueManager.claimNextTask();
        Assertions.assertTrue(jobResult2 != null);
        Task jobResult3 = queueManager.claimNextTask();
        Assertions.assertTrue(jobResult3 != null);
        Task jobResult4 = queueManager.claimNextTask();
        Assertions.assertTrue(jobResult4 == null);
    }

    @Test
    public void completeJob1() {
        Task job1 = queueManager.insertTask("REPORT~1234", "REPORT");
        Task jobResult1 = queueManager.claimNextTask();
        Assertions.assertTrue(jobResult1 != null);
        queueManager.completeTask(jobResult1);
    }

    @Test
    public void completeJob2() {
        Assertions.assertThrows(AggregationQueueException.class, () -> {
            Task job1 = queueManager.insertTask("REPORT~1234", "REPORT");
            Task jobResult1 = queueManager.claimNextTask();
            Assertions.assertTrue(jobResult1 != null);
            queueManager.completeTask(jobResult1);
            Assertions.assertTrue(true);
            queueManager.completeTask(jobResult1);
            Assertions.assertTrue(false);
        });
    }
}
