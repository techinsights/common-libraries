package io.pythagoras.common.aggregationqueue;

public interface AggregationQueueService {

    Task addTask(String taskType, String targetId);

    Task claimNext(String taskType);

    Task claimNext();

    void complete(Task task);

    void complete(int taskId);

    long querySize();
}
