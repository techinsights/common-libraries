package io.pythagoras.common.aggregationqueue;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(value = "io.pythagoras.aggregationqueue")
public class AggregationQueueProperties {

    private int offset = 1800;

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
}
