package io.pythagoras.common.aggregationqueue;

public class AggregationQueueException extends RuntimeException {

    public AggregationQueueException(String message) {
        super(message);
    }

    public AggregationQueueException(String message, Throwable cause) {
        super(message, cause);
    }
}
