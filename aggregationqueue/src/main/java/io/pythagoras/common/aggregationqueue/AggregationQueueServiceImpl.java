package io.pythagoras.common.aggregationqueue;

import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AggregationQueueServiceImpl implements AggregationQueueService {


    private QueueManager queueManager;
    private AggregationQueueProperties aggregationQueueProperties;

    private final Logger logger = LoggerFactory.getLogger(AggregationQueueServiceImpl.class);

    @Autowired
    public void setQueueManager(QueueManager queueManager) {
        this.queueManager = queueManager;
    }

    @Autowired
    public void setAggregationQueueProperties(AggregationQueueProperties aggregationQueueProperties) {
        this.aggregationQueueProperties = aggregationQueueProperties;
    }

    @PostConstruct
    public void postConstruct() {

        try {
            queueManager.createStoredProcedures();
        } catch (Exception e) {
            logger.warn("Aggregation queue could not create stored procedures: {}", e.getMessage(), e);
        }

        queueManager.setOffset(aggregationQueueProperties.getOffset());
    }

    @Override
    public Task addTask(String taskType, String targetId) {
        return queueManager.insertTask(Task.makeCode(taskType, targetId), taskType);
    }

    @Override
    public Task claimNext(String taskType) {
        return queueManager.claimNextTaskByType(taskType);
    }

    @Override
    public Task claimNext() {
        return queueManager.claimNextTask();
    }

    @Override
    public void complete(Task task) {
        queueManager.completeTask(task);
    }

    @Override
    public void complete(int taskId) {
        queueManager.completeTask(taskId);
    }

    @Override
    public long querySize() { return queueManager.querySize(); }
}
