package io.pythagoras.common.aggregationqueue;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import org.hibernate.annotations.ColumnDefault;
import java.util.Date;

@Entity
@Table(name = "ipcag_task", uniqueConstraints = {
        @UniqueConstraint(name = "ipcag_task_code_timestmp", columnNames = {"code","timestmp"})
})
public class Task {

    private final static String SEPARATOR = "~";

    @Column(name="id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="code")
    @NotNull
    private String code;

    @Column(name="task_type")
    @NotNull
    private String taskType;

    @Column(name = "timestmp", columnDefinition = "timestamp")
    @NotNull
    private Date timestamp;

    @Column(name="randomizer")
    private String randomizer;

    @Column(name="request_count")
    private int requestCount;

    @Column(name="execution_count", nullable = false)
    @ColumnDefault("0")
    private int executionCount;

    public int getId() {
        return id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getTaskType() {
        return taskType;
    }

    public String getTargetId() {
        return code.split(SEPARATOR)[1];
    }

    public void setCode(String taskType, String targetId) {
        this.code = makeCode(taskType,targetId);
        this.taskType = taskType;
    }

    public static String makeCode(String taskType, String targetId) {
        return taskType + SEPARATOR + targetId;
    }
}
