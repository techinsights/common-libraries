package io.pythagoras.common.aggregationqueue;

import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.math.BigInteger;
import java.util.Date;
import java.util.UUID;

@Service
public class QueueManager {

    private static final String CLAIM_ANY_TASK_FUNCTION =
            "CREATE OR REPLACE FUNCTION aggregation_queue_claim_any_task(offset_time integer)\n" +
                    "    RETURNS TABLE\n" +
                    "            (\n" +
                    "                id              integer,\n" +
                    "                code            varchar(255),\n" +
                    "                execution_count integer,\n" +
                    "                randomizer      varchar(255),\n" +
                    "                request_count   integer,\n" +
                    "                task_type       varchar(255),\n" +
                    "                timestmp        timestamp(6)\n" +
                    "            )\n" +
                    "AS\n" +
                    "$$\n" +
                    "DECLARE\n" +
                    "    task_id int;\n" +
                    "BEGIN\n" +
                    "    LOCK TABLE ipcag_task IN ACCESS EXCLUSIVE MODE;\n" +
                    "\n" +
                    "    SELECT ipcag_task.id\n" +
                    "    INTO task_id\n" +
                    "    FROM ipcag_task\n" +
                    "    WHERE ipcag_task.timestmp < CAST((now() - offset_time * INTERVAL '1 SECOND') as TIMESTAMP)\n" +
                    "      AND ipcag_task.code NOT IN (\n" +
                    "        SELECT ipcag_task.code\n" +
                    "        FROM ipcag_task\n" +
                    "        WHERE ipcag_task.timestmp >= CAST((now() - offset_time * INTERVAL '1 SECOND') as TIMESTAMP))\n" +
                    "    LIMIT 1;\n" +
                    "\n" +
                    "    UPDATE ipcag_task\n" +
                    "    SET timestmp        = now(),\n" +
                    "        execution_count = (ipcag_task.execution_count + 1)\n" +
                    "    WHERE ipcag_task.id = task_id;\n" +
                    "\n" +
                    "    RETURN QUERY\n" +
                    "        SELECT * FROM ipcag_task WHERE ipcag_task.id = task_id;\n" +
                    "END\n" +
                    "$$ LANGUAGE plpgsql;";

    private static final String CLAIM_ANY_TASK_BY_TYPE_FUNCTION =
            "CREATE OR REPLACE FUNCTION aggregation_queue_claim_task_by_type(offset_time integer, type_input varchar)\n" +
                    "    RETURNS TABLE\n" +
                    "            (\n" +
                    "                id              integer,\n" +
                    "                code            varchar(255),\n" +
                    "                execution_count integer,\n" +
                    "                randomizer      varchar(255),\n" +
                    "                request_count   integer,\n" +
                    "                task_type       varchar(255),\n" +
                    "                timestmp        timestamp(6)\n" +
                    "            )\n" +
                    "AS\n" +
                    "$$\n" +
                    "DECLARE\n" +
                    "    task_id int;\n" +
                    "BEGIN\n" +
                    "    LOCK TABLE ipcag_task IN ACCESS EXCLUSIVE MODE;\n" +
                    "\n" +
                    "    SELECT ipcag_task.id\n" +
                    "    INTO task_id\n" +
                    "    FROM ipcag_task\n" +
                    "    WHERE ipcag_task.timestmp < CAST((now() - offset_time * INTERVAL '1 SECOND') as TIMESTAMP)\n" +
                    "      AND ipcag_task.task_type = type_input\n" +
                    "      AND ipcag_task.code NOT IN (\n" +
                    "        SELECT ipcag_task.code\n" +
                    "        FROM ipcag_task\n" +
                    "        WHERE ipcag_task.timestmp >= CAST((now() - offset_time * INTERVAL '1 SECOND') as TIMESTAMP))\n" +
                    "    LIMIT 1;\n" +
                    "\n" +
                    "    UPDATE ipcag_task\n" +
                    "    SET timestmp        = now(),\n" +
                    "        execution_count = (ipcag_task.execution_count + 1)\n" +
                    "    WHERE ipcag_task.id = task_id;\n" +
                    "\n" +
                    "    RETURN QUERY\n" +
                    "        SELECT * FROM ipcag_task WHERE ipcag_task.id = task_id;\n" +
                    "END\n" +
                    "$$ LANGUAGE plpgsql;";

    private EntityManager entityManager;

    private int offset = 1800; // Default 30 min

    @Autowired
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    @Transactional
    public void createStoredProcedures() {
        entityManager.createNativeQuery(CLAIM_ANY_TASK_FUNCTION).executeUpdate();
        entityManager.createNativeQuery(CLAIM_ANY_TASK_BY_TYPE_FUNCTION).executeUpdate();
    }

    public Task insertTask(String code, String taskType) {

        Query query = entityManager.createNativeQuery("INSERT INTO ipcag_task (code,task_type, timestmp, randomizer, request_count, execution_count) " +
                "VALUES (:code, :taskType, :timestmp, :randomizer, 1, 0) ON CONFLICT ON CONSTRAINT code_timestmp " +
                "DO UPDATE SET randomizer = :randomizer , request_count = ( ipcag_task.request_count + 1 ) RETURNING *", Task.class);

        query.setParameter("code", code);
        query.setParameter("taskType", taskType);
        query.setParameter("timestmp", new Date(0));
        query.setParameter("randomizer", UUID.randomUUID().toString());

        return (Task) query.getSingleResult();
    }

    public Task claimNextTask() {
        return processClaimNextTask(makeQueryForClaimAnyTask());
    }

    public Task claimNextTaskByType(String taskType) {
        return processClaimNextTask(makeQueryForClaimTaskByType(taskType));
    }

    Task processClaimNextTask(Query query) {
        try {
            return (Task) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    Query makeQueryForClaimAnyTask() {
        return entityManager.createNativeQuery("SELECT * FROM aggregation_queue_claim_any_task(:offset)", Task.class)
                .setParameter("offset", offset);
    }

    Query makeQueryForClaimTaskByType(String taskType) {
        return entityManager.createNativeQuery("SELECT * FROM aggregation_queue_claim_task_by_type(:offset, :taskType)", Task.class)
                .setParameter("offset", offset)
                .setParameter("taskType", taskType);
    }

    public void completeTask(Task task) throws AggregationQueueException {
        completeTask(task.getId());
    }

    public void completeTask(int taskId) throws AggregationQueueException {
        Query query = entityManager.createNativeQuery("DELETE FROM ipcag_task WHERE id = :id RETURNING id")
                .setParameter("id", taskId);

        try {
            Integer result = (Integer) query.getSingleResult();
            if (!result.equals(taskId)) {
                throw new AggregationQueueException("Completed unexpected job.  Expected: " + taskId + " Completed: " + result);
            }
        } catch (NoResultException e) {
            throw new AggregationQueueException("Unable to complete job: " + taskId, e);
        }
    }

    public long querySize() {
        Query query = entityManager.createNativeQuery("SELECT count(*) FROM ipcag_task");

        try {
            BigInteger result = (BigInteger) query.getSingleResult();
            if (null == result) {
                throw new NoResultException("Result Size is null");
            }
            return result.longValue();
        } catch (NoResultException e) {
            throw new AggregationQueueException("Unable to get queue size.", e);
        }
    }
}
