package io.pythagoras.common.aggregationqueue;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(value = AggregationQueueProperties.class)
public class AggregationQueuePropertiesConfiguration {
}
