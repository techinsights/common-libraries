package io.pythagoras.common.s3eventqueue;

import com.amazonaws.services.sqs.model.Message;
import io.pythagoras.common.s3eventqueue.classes.Record;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

public class S3EventQueueTest {

    private S3EventQueue s3EventQueue;

    @BeforeEach
    public void setupOnce() {
        s3EventQueue = new S3EventQueue(new S3EventProcessorInterface() {
            @Override
            public void handleEvent(Record record) {

            }
        }, LoggerFactory.getLogger("TEST"),"access","secret","us-east-1","",true);
    }


    @Test
    public void unwrapSqsJson_1() throws Exception {

        String message = "{\"messageId\":\"64cd1e25-aa6e-4a9c-897d-bf39571c9d0d\",\"receiptHandle\":\"AQEBJTOhYhePjvoJzBSilVuTv5SO+O3C3JLYcE4e7jnf0SdFy5eQq2vlIIlnOxnxvrethblx0QrW8ujCE4aal0suSojX2StHh9FHQZx0lmf9oxCTb/ieMvEdMl/Yw3vDe9sGG5YCSiZAqMjBq3UAnsX+a+KFddgFJ7X14L1HcpSzFLaqDSYwaRwwQiik84dE97USVOfJsODCGljqnMtfjf8mWofOpMvir6k05Gm8S2v1We4QGilJFI6mmzO5/Z/XWJw1tRK0roAFGZkuhi8ED0eecQaFvcTI+hYpGXXf1UE9SLcbO3WLM5wYXEQX02dLPdQeHJtDXXBAg1bgGU5D/nWHKjqbaDmMCg4WT1fBZyPUTbBA/gO5WjDkCrqBkr5PkLeq0YTfvbpdkzxgXZh4LgYUHiQWHMQpc/WBbyB7MudJbeM\\u003d\",\"mD5OfBody\":\"7f26316469cbd6b0162e97cd3ac02f40\",\"body\":\"{\\n  \\\"Type\\\" : \\\"Notification\\\",\\n  \\\"MessageId\\\" : \\\"afa89376-0976-5cf3-9f67-c643575037ea\\\",\\n  \\\"TopicArn\\\" : \\\"arn:aws:sns:us-east-2:661145040624:ti-brs-chunks-useast2-int2-events\\\",\\n  \\\"Subject\\\" : \\\"Amazon S3 Notification\\\",\\n  \\\"Message\\\" : \\\"{\\\\\\\"Records\\\\\\\":[{\\\\\\\"eventVersion\\\\\\\":\\\\\\\"2.1\\\\\\\",\\\\\\\"eventSource\\\\\\\":\\\\\\\"aws:s3\\\\\\\",\\\\\\\"awsRegion\\\\\\\":\\\\\\\"us-east-2\\\\\\\",\\\\\\\"eventTime\\\\\\\":\\\\\\\"2019-04-20T17:50:30.737Z\\\\\\\",\\\\\\\"eventName\\\\\\\":\\\\\\\"ObjectCreated:Put\\\\\\\",\\\\\\\"userIdentity\\\\\\\":{\\\\\\\"principalId\\\\\\\":\\\\\\\"AWS:AIDAIMXVAYQEF3W2EOOCG\\\\\\\"},\\\\\\\"requestParameters\\\\\\\":{\\\\\\\"sourceIPAddress\\\\\\\":\\\\\\\"18.221.50.243\\\\\\\"},\\\\\\\"responseElements\\\\\\\":{\\\\\\\"x-amz-request-id\\\\\\\":\\\\\\\"37512F3F2928EBDC\\\\\\\",\\\\\\\"x-amz-id-2\\\\\\\":\\\\\\\"8Rr2W9KKdW71rO0KJZEEET3xvFEPDiHcKfl8cy0xiSkh7CSdnyGFAhYM78ibzU9EU1dY3fCCRQI\\u003d\\\\\\\"},\\\\\\\"s3\\\\\\\":{\\\\\\\"s3SchemaVersion\\\\\\\":\\\\\\\"1.0\\\\\\\",\\\\\\\"configurationId\\\\\\\":\\\\\\\"Create Events\\\\\\\",\\\\\\\"bucket\\\\\\\":{\\\\\\\"name\\\\\\\":\\\\\\\"ti-brs-chunks-useast2-int2\\\\\\\",\\\\\\\"ownerIdentity\\\\\\\":{\\\\\\\"principalId\\\\\\\":\\\\\\\"A309WPLLKM3B4V\\\\\\\"},\\\\\\\"arn\\\\\\\":\\\\\\\"arn:aws:s3:::ti-brs-chunks-useast2-int2\\\\\\\"},\\\\\\\"object\\\\\\\":{\\\\\\\"key\\\\\\\":\\\\\\\"eaa33e19600c594139db058115090d244202d1c0d1b8262365699c510f3e61dc\\\\\\\",\\\\\\\"size\\\\\\\":3751610,\\\\\\\"eTag\\\\\\\":\\\\\\\"885a40d9932cb96e738f1f2f12c7c1a7\\\\\\\",\\\\\\\"sequencer\\\\\\\":\\\\\\\"005CBB5BE117E59320\\\\\\\"}}}]}\\\",\\n  \\\"Timestamp\\\" : \\\"2019-04-20T17:50:30.952Z\\\",\\n  \\\"SignatureVersion\\\" : \\\"1\\\",\\n  \\\"Signature\\\" : \\\"URB6qRzq3K+s+URmX+pCD9D+X2bwmqEALBUUWZVAXliBktuEIdc8B7kzf1pcSYBWHXZSmUCLtmwnknsKY5MAhTpBGpC3SA756ytz304FsKGmnOLAJTxB0QFt2em59M7utM6HPz4ctcZe6wjTRAMkeGjBL1MwS6ilPmBsP9YscnwzBTRls3z1dMYZeMD5zCG8VE23M8NSs91PnCUVzVH0Nrp8qCiiTfu6E+kvc0RaJfS5dv9ETWqCcXMSEEEJDbPdbXM9Cqy6RNTlC1/hoWqPtOqjvmVtTAYEmTcHVo7+TCL/i+Wl8F5gkNooERAWiN4JYTGa1BMOV6My/Dm9yt8fxg\\u003d\\u003d\\\",\\n  \\\"SigningCertURL\\\" : \\\"https://sns.us-east-2.amazonaws.com/SimpleNotificationService-6aad65c2f9911b05cd53efda11f913f9.pem\\\",\\n  \\\"UnsubscribeURL\\\" : \\\"https://sns.us-east-2.amazonaws.com/?Action\\u003dUnsubscribe\\u0026SubscriptionArn\\u003darn:aws:sns:us-east-2:661145040624:ti-brs-chunks-useast2-int2-events:4fcb5c59-2c1b-4ae4-8da8-77c633d8a50e\\\"\\n}\",\"messageAttributes\":{}}\n";

        Message message1 = s3EventQueue.jsonToSqsMessage(message);

        Assertions.assertNotNull(message1);

    }

    @Test
    public void unwrapSNSMessage_1() throws Exception {
        String message = "{\n" +
                "  \"Type\" : \"Notification\",\n" +
                "  \"MessageId\" : \"afa89376-0976-5cf3-9f67-c643575037ea\",\n" +
                "  \"TopicArn\" : \"arn:aws:sns:us-east-2:661145040624:ti-brs-chunks-useast2-int2-events\",\n" +
                "  \"Subject\" : \"Amazon S3 Notification\",\n" +
                "  \"Message\" : \"{\\\"Records\\\":[{\\\"eventVersion\\\":\\\"2.1\\\",\\\"eventSource\\\":\\\"aws:s3\\\",\\\"awsRegion\\\":\\\"us-east-2\\\",\\\"eventTime\\\":\\\"2019-04-20T17:50:30.737Z\\\",\\\"eventName\\\":\\\"ObjectCreated:Put\\\",\\\"userIdentity\\\":{\\\"principalId\\\":\\\"AWS:AIDAIMXVAYQEF3W2EOOCG\\\"},\\\"requestParameters\\\":{\\\"sourceIPAddress\\\":\\\"18.221.50.243\\\"},\\\"responseElements\\\":{\\\"x-amz-request-id\\\":\\\"37512F3F2928EBDC\\\",\\\"x-amz-id-2\\\":\\\"8Rr2W9KKdW71rO0KJZEEET3xvFEPDiHcKfl8cy0xiSkh7CSdnyGFAhYM78ibzU9EU1dY3fCCRQI=\\\"},\\\"s3\\\":{\\\"s3SchemaVersion\\\":\\\"1.0\\\",\\\"configurationId\\\":\\\"Create Events\\\",\\\"bucket\\\":{\\\"name\\\":\\\"ti-brs-chunks-useast2-int2\\\",\\\"ownerIdentity\\\":{\\\"principalId\\\":\\\"A309WPLLKM3B4V\\\"},\\\"arn\\\":\\\"arn:aws:s3:::ti-brs-chunks-useast2-int2\\\"},\\\"object\\\":{\\\"key\\\":\\\"eaa33e19600c594139db058115090d244202d1c0d1b8262365699c510f3e61dc\\\",\\\"size\\\":3751610,\\\"eTag\\\":\\\"885a40d9932cb96e738f1f2f12c7c1a7\\\",\\\"sequencer\\\":\\\"005CBB5BE117E59320\\\"}}}]}\",\n" +
                "  \"Timestamp\" : \"2019-04-20T17:50:30.952Z\",\n" +
                "  \"SignatureVersion\" : \"1\",\n" +
                "  \"Signature\" : \"URB6qRzq3K+s+URmX+pCD9D+X2bwmqEALBUUWZVAXliBktuEIdc8B7kzf1pcSYBWHXZSmUCLtmwnknsKY5MAhTpBGpC3SA756ytz304FsKGmnOLAJTxB0QFt2em59M7utM6HPz4ctcZe6wjTRAMkeGjBL1MwS6ilPmBsP9YscnwzBTRls3z1dMYZeMD5zCG8VE23M8NSs91PnCUVzVH0Nrp8qCiiTfu6E+kvc0RaJfS5dv9ETWqCcXMSEEEJDbPdbXM9Cqy6RNTlC1/hoWqPtOqjvmVtTAYEmTcHVo7+TCL/i+Wl8F5gkNooERAWiN4JYTGa1BMOV6My/Dm9yt8fxg==\",\n" +
                "  \"SigningCertURL\" : \"https://sns.us-east-2.amazonaws.com/SimpleNotificationService-6aad65c2f9911b05cd53efda11f913f9.pem\",\n" +
                "  \"UnsubscribeURL\" : \"https://sns.us-east-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-2:661145040624:ti-brs-chunks-useast2-int2-events:4fcb5c59-2c1b-4ae4-8da8-77c633d8a50e\"\n" +
                "}";

        S3EventQueue.SNSNotification body = s3EventQueue.jsonToSnsMessage(message);

        Assertions.assertNotNull(body);
    }

    @Test
    public void jsonToS3Notification() throws Exception {
        String json = "{\"Records\":[{\"eventVersion\":\"2.1\",\"eventSource\":\"aws:s3\",\"awsRegion\":\"us-east-2\",\"eventTime\":\"2019-04-20T17:50:30.737Z\",\"eventName\":\"ObjectCreated:Put\",\"userIdentity\":{\"principalId\":\"AWS:AIDAIMXVAYQEF3W2EOOCG\"},\"requestParameters\":{\"sourceIPAddress\":\"18.221.50.243\"},\"responseElements\":{\"x-amz-request-id\":\"37512F3F2928EBDC\",\"x-amz-id-2\":\"8Rr2W9KKdW71rO0KJZEEET3xvFEPDiHcKfl8cy0xiSkh7CSdnyGFAhYM78ibzU9EU1dY3fCCRQI=\"},\"s3\":{\"s3SchemaVersion\":\"1.0\",\"configurationId\":\"Create Events\",\"bucket\":{\"name\":\"ti-brs-chunks-useast2-int2\",\"ownerIdentity\":{\"principalId\":\"A309WPLLKM3B4V\"},\"arn\":\"arn:aws:s3:::ti-brs-chunks-useast2-int2\"},\"object\":{\"key\":\"eaa33e19600c594139db058115090d244202d1c0d1b8262365699c510f3e61dc\",\"size\":3751610,\"eTag\":\"885a40d9932cb96e738f1f2f12c7c1a7\",\"sequencer\":\"005CBB5BE117E59320\"}}}]}";
        S3EventQueue.S3Notification body = s3EventQueue.jsonToS3Notification(json);

        Assertions.assertNotNull(body);
    }
}
