package io.pythagoras.common.s3eventqueue;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Base64;

public class ByteBufferTypeAdapter extends TypeAdapter<ByteBuffer> {
    @Override
    public void write(JsonWriter out, ByteBuffer value) throws IOException {
        out.value(Base64.getEncoder().encodeToString(value.array()));
    }

    @Override
    public ByteBuffer read(JsonReader in) throws IOException {
        byte[] bytes = Base64.getDecoder().decode(in.nextString());
        return ByteBuffer.wrap(bytes);
    }
}
