package io.pythagoras.common.s3eventqueue.classes;

import java.util.Date;

public class Record {
    public String eventVersion;
    public String eventSource;
    public String awsRegion;
    public Date eventTime;
    public String eventName;
    public UserIdentity userIdentity;
    public RequestParameters requestParameters;
    public S3NS3 s3;
}
