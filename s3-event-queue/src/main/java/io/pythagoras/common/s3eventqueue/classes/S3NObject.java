package io.pythagoras.common.s3eventqueue.classes;

public class S3NObject {
    public String key;
    public Long size;
    public String eTag;
    public String versionId;
    public String sequencer;
}
