package io.pythagoras.common.s3eventqueue;

import io.pythagoras.common.s3eventqueue.classes.Record;

public interface S3EventProcessorInterface {

    void handleEvent(Record record);
}
