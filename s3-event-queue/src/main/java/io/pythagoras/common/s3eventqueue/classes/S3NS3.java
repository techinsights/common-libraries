package io.pythagoras.common.s3eventqueue.classes;

public class S3NS3 {
    public String s3SchemaVersion;
    public String configurationId;
    public S3NBucket bucket;
    public S3NObject object;
}
