package io.pythagoras.common.s3eventqueue.classes;

public class S3NBucket {
    public String name;
    public UserIdentity ownerIdentity;
    public String arn;
}
