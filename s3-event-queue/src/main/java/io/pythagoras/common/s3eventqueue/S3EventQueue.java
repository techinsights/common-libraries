package io.pythagoras.common.s3eventqueue;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.pythagoras.common.s3eventqueue.classes.Record;
import org.slf4j.Logger;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class S3EventQueue {
    public static final String S3E_TestEvent = "s3:TestEvent";
    public static final String S3E_ObjectCreated_Put = "s3:ObjectCreated:Put";
    public static final String S3E_ObjectCreated_Post = "s3:ObjectCreated:Post";
    public static final String S3E_ObjectCreated_Copy = "s3:ObjectCreated:Copy";
    public static final String S3E_ObjectCreated_CompleteMultipartUpload = "s3:ObjectCreated:CompleteMultipartUpload";
    public static final String S3E_ObjectRemoved_Delete = "s3:ObjectRemoved:Delete";
    public static final String S3E_ObjectRemoved_DeleteMarkerCreated = "s3:ObjectRemoved:DeleteMarkerCreated";
    public static final String S3E_ReducedRedundancyLostObject = "s3:ReducedRedundancyLostObject";

    private Logger logger;

    private AmazonSQS sqs;

    private String queueUrl;

    private Integer parallelReceivers = 4;

    private boolean started = false;

    private S3EventProcessorInterface eventProcessor;

    private boolean isSNStoSQS = false;

    public S3EventQueue(S3EventProcessorInterface eventProcessor, Logger logger, String awsAccessKey, String awsSecretKey, String region, String queueUrl, Boolean isSNStoSQS) {

        this.isSNStoSQS = isSNStoSQS;
        this.queueUrl = queueUrl;
        this.logger = logger;
        this.eventProcessor = eventProcessor;

        this.sqs = AmazonSQSClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(
                        new BasicAWSCredentials(awsAccessKey, awsSecretKey)))
                .withRegion(region)
                .build();
    }

    public void setParallelReceivers(Integer parallelReceivers) throws S3EventQueueException {
        if (started) {
            throw new S3EventQueueException("Cannot change number of parallel receivers once SQSConsumer is started.");
        }
        if (parallelReceivers > 20) {
            throw new S3EventQueueException("Lets not be crazy. " + parallelReceivers + " parallel receivers is a lot.  Keep it to 20 or less.");
        }
        this.parallelReceivers = parallelReceivers;
    }

    public void start() {

        if (!started) {
            logger.info("Starting");
            started = true;

            ScheduledExecutorService executor = Executors.newScheduledThreadPool(parallelReceivers);

            Runnable task = () -> {
                this.receiveMessages();
            };

            for (int i = 0; i < parallelReceivers; i++) {
                executor.scheduleWithFixedDelay(task, 0, 1, TimeUnit.MILLISECONDS);
            }
        }

    }

    private void receiveMessages() {

        ReceiveMessageRequest request = new ReceiveMessageRequest();
        request.setQueueUrl(this.queueUrl);
        request.setMaxNumberOfMessages(1);
        request.setWaitTimeSeconds(20);

        ReceiveMessageResult result = this.sqs.receiveMessage(request);

        for (Message message : result.getMessages()) {
            // Handle the message and delete on success
            logger.info("S3 Change Detected: ",message);
            try {
                this.handleSQSMessage(message);
                sqs.deleteMessage(new DeleteMessageRequest(this.queueUrl, message.getReceiptHandle()));
            } catch (Exception e) {
                Gson gson = new Gson();
                String messageStr = gson.toJson(message);
                logger.error("Unable to fully process notification message from S3: " +
                        e.getMessage() + "\n" + messageStr, e);
            }
        }

    }

    void handleSQSMessage(Message message) throws S3EventQueueException {

        String s3NotificationJson = message.getBody();

        if(this.isSNStoSQS) {
            s3NotificationJson = this.jsonToSnsMessage(message.getBody()).Message;
        }

        S3Notification s3Notification = this.jsonToS3Notification(s3NotificationJson);

        this.handleMessage(s3Notification);
    }


    Message jsonToSqsMessage(String messageJson) throws S3EventQueueException {
        Gson gson = createCustomGson();
        try {
            Message message = gson.fromJson(messageJson, Message.class);
            if (message == null) {
                throw new RuntimeException("Unable to unwrap events from SNS.");
            }
            return message;
        } catch (Exception e) {
            throw new S3EventQueueException("Error handling S3 Watcher message.", e);
        }
    }

    private Gson createCustomGson() {
        return new GsonBuilder()
                .registerTypeAdapter(ByteBuffer.class, new ByteBufferTypeAdapter())
                .create();
    }

    SNSNotification jsonToSnsMessage(String messageJson) throws S3EventQueueException {
        Gson gson = new Gson();
        try {
            SNSNotification snsNotification = gson.fromJson(messageJson, SNSNotification.class);
            if (snsNotification == null) {
                throw new RuntimeException("Unable to unwrap events from SNS.");
            }
            return snsNotification;
        } catch (Exception e) {
            throw new S3EventQueueException("Error handling S3 Watcher message.", e);
        }
    }

    S3Notification jsonToS3Notification(String body) throws S3EventQueueException {
        Gson gson = new Gson();

        logger.info("Unwrapping SNS Message.");
        try {
            S3Notification s3Notification = gson.fromJson(body, S3Notification.class);
            if (s3Notification == null || ( s3Notification.Event == null && s3Notification.Records==null)) {
                throw new RuntimeException("Unable to unwrap events from SNS.");
            }
            return s3Notification;
        } catch (Exception e) {
            throw new S3EventQueueException("Error handling S3 Watcher message.", e);
        }
    }


    private void handleMessage(S3Notification notification) throws S3EventQueueException {


        // Check if it is a test message.
        if (notification.Event != null && notification.Event.equals(S3E_TestEvent)) {
            logger.info("S3 Test event received.");
            return;
        } else if (notification.Records == null) {
            throw new S3EventQueueException("Not a test, not a real message.  I'm confused.");
        }

        // Looks like we need to unwrap the event records now.
        logger.info("Looping notification records.");
        for (Record record : notification.Records) {
            handleRecord(record);
        }

    }

    void handleRecord(Record record) {

        this.eventProcessor.handleEvent(record);
    }

    class SNSNotification {
        String Type;
        String MessageId;
        String TopicArn;
        String Subject;
        String Message;
        String Timestamp;
    }

    class S3Notification {
        List<Record> Records = new ArrayList<>();
        String Event;
    }
}
