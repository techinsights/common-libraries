package io.pythagoras.common.s3eventqueue;

public class S3EventQueueException extends Exception {

    public S3EventQueueException(String message) {
        super(message);
    }

    public S3EventQueueException(String message, Throwable cause) {
        super(message, cause);
    }
}
