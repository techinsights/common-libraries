#S3 Event Queue
This allows for the consuming of an SQS Queue which is being populated by AWS S3 Events.

## Usage
Create a handler which implements the `S3EventProcessorInterface`.  This class will get called on every event that comes in.

You then pass the implementation class into the constructor for `S3EventQueue` along with a logger implementation, aws credentials, and the url of the queue to consume.

You can then set the number of parallel receivers using `setParallelReceivers(int)`.  Up to 20 parallel receivers can be used.  If more are desired, then launch multiple instances.

Finally, you can start the consumer using `start()`