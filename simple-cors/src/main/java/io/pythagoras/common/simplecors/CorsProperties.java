package io.pythagoras.common.simplecors;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("pythagoras.cors")
public class CorsProperties {

    private String allowOrigin = "*";
    private String allowMethods = "POST, GET, PUT, OPTIONS, DELETE, PATCH";
    private String maxAge = "1728000";
    private String allowHeaders = "Content-Type, x-requested-with, authorization";
    private String allowCredentials = "false";

    public String getAllowOrigin() {
        return allowOrigin;
    }

    public void setAllowOrigin(String allowOrigin) {
        this.allowOrigin = allowOrigin;
    }

    public void setAllowMethods(String allowMethods) {
        this.allowMethods = allowMethods;
    }

    public String getAllowMethods() {
        return allowMethods;
    }

    public String getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(String maxAge) {
        this.maxAge = maxAge;
    }

    public String getAllowHeaders() {
        return allowHeaders;
    }

    public void setAllowHeaders(String allowHeaders) {
        this.allowHeaders = allowHeaders;
    }

    public String getAllowCredentials() { return allowCredentials; }

    public void setAllowCredentials(String allowCredentials) { this.allowCredentials = allowCredentials; }
}
