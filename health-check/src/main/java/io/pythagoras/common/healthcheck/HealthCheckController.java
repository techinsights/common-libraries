package io.pythagoras.common.healthcheck;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/util")
public class HealthCheckController {

    private HealthCheckServiceImpl healthCheckService;

    @Autowired
    public void setHealthCheckService(HealthCheckServiceImpl healthCheckService) {
        this.healthCheckService = healthCheckService;
    }

    @Value(value = "${spring.application.name}")
    private String APP_NAME;

    @RequestMapping(value="/health", method = RequestMethod.GET)
    public ResponseEntity<?> healthCheck() {
        if(healthCheckService.checkIsGood()) {
            return ResponseEntity.status(HttpStatus.OK).body("OK");
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("NOT OK");
    }

    @RequestMapping(value = "/who", method = RequestMethod.GET)
    public String name() {
        return APP_NAME;
    }
}

