package io.pythagoras.common.healthcheck;

public interface HealthCheckService {

    void setIsGood();

    void setIsNotGood();

    Boolean checkIsGood();
}
