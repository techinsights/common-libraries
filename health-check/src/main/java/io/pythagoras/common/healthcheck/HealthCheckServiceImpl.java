package io.pythagoras.common.healthcheck;

import org.springframework.stereotype.Service;

@Service
public class HealthCheckServiceImpl implements HealthCheckService {

    private Boolean isGood = true;

    @Override
    public void setIsGood() {
        isGood = true;
    }

    @Override
    public void setIsNotGood() {
        isGood = false;
    }

    @Override
    public Boolean checkIsGood() {
        return isGood;
    }
}
