package io.pythagoras.common.loggercommon;

import com.google.gson.Gson;

public class LogObject {
    public Integer statusCode;
    public Exception exception;

    @Override
    public String toString() {
        Gson gson = new Gson();

        return gson.toJson(this);
    }
}
