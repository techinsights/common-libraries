package io.pythagoras.common.distributedsequenceidgenerator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.IntStream;

public class GeneratorTests {

    @Test
    public void getNewId() throws IdGenerationException, ParseException {
        Generator gen = Generator.create(1,3);

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date dte = format.parse("2019-04-23T01:45:23.123Z");

        String id = gen.getNewId(dte);
        String id2 = gen.getNewId(dte);

        Assertions.assertNotNull(id);
        Assertions.assertEquals("1555983923123000001", id);
        Assertions.assertNotNull(id2);
        Assertions.assertEquals("1555983923123001001", id2);

    }

    @Test
    public void getNewId_small() throws IdGenerationException, ParseException {
        Generator gen = Generator.create(1,3);

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date dte = format.parse("1970-04-23T01:45:23.123Z");

        String id = gen.getNewId(dte);

        Assertions.assertNotNull(id);
        Assertions.assertEquals("0009683123123000001", id);
    }

    @Test
    public void getNewLongId() throws IdGenerationException, ParseException {
        Generator gen = Generator.create(1,3);

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date dte = format.parse("2019-04-23T01:45:23.123Z");

        Long id = gen.getNewLongId(dte);
        Long id2 = gen.getNewLongId(dte);

        Assertions.assertNotNull(id);
        Assertions.assertEquals(1555983923123000001L, id.longValue());
        Assertions.assertNotNull(id2);
        Assertions.assertEquals(1555983923123001001L, id2.longValue());

    }

    @Test
    public void getNewLongId_wayInFuture() throws IdGenerationException, ParseException {
        Generator gen = Generator.create(1,3);

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date dte = format.parse("2262-04-11T23:47:16.854-0000");
        Date dte2 = format.parse("2262-04-11T23:47:16.855-0000");

        try {
            Long id = gen.getNewLongId(dte);
        } catch (Exception e) {
            Assertions.assertFalse(true);
        }

        try {
            Long id = gen.getNewLongId(dte2);
        } catch (Exception e) {
            Assertions.assertEquals(IdGenerationException.class, e.getClass());
            return;
        }

        Assertions.assertTrue(false);
    }

    @Test
    public void getDifferentIds() throws IdGenerationException {
        Generator generator = Generator.create(1, 100);

        Set<String> results = new HashSet<>();
        List<CompletableFuture<String>> tasks = new ArrayList<>();

        IntStream.range(0, 100).forEach((num) ->
                tasks.add(CompletableFuture.supplyAsync(() -> {
                    try {
                        return generator.getNewId(new Date().toInstant());
                    } catch (IdGenerationException e) {
                        return "ERROR";
                    }
                }))
        );

        tasks.forEach((task) -> {
            try {
                results.add(task.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });

        Assertions.assertFalse(results.contains("ERROR"));
        Assertions.assertEquals(tasks.size(), results.size());
    }
    
    @Test
    public void create_valid() throws Exception {
        Generator.create(0,400);
        Generator.create(1,400);
        Generator.create(500,400);
        Generator.create(999,400);
    }

    @Test
    public void create_fail() throws Exception {
        try {
            Generator.create(1000, 400);
        } catch (IdGenerationException e) {
            return;
        }
        Assertions.fail("Did not throw exception");
    }
}
