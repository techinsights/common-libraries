package io.pythagoras.common.distributedsequenceidgenerator;

public class IdGenerationException extends Exception {
    public IdGenerationException(String message) {
        super(message);
    }

    public IdGenerationException(String message, Throwable cause) {
        super(message, cause);
    }
}
