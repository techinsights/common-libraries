package io.pythagoras.common.distributedsequenceidgenerator;

import java.time.Instant;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

public class Generator {

    private final String instanceId;
    private final Map<Long, Integer> history;

    public static Generator create(Integer generatorInstanceId, Integer maxHistoryNumber) throws IdGenerationException {
        if(generatorInstanceId > 999 || generatorInstanceId < 0) {
            throw new IdGenerationException("generatorInstanceId must be between 0 and 999 inclusive");
        }
        return new Generator(String.format ("%03d", generatorInstanceId), maxHistoryNumber);
    }

    private Generator(String instanceId, Integer maxHistoryNumber) {
        this.instanceId = instanceId;
        this.history = Collections.synchronizedMap(new FifoMap<Long, Integer>(maxHistoryNumber) {
            @Override
            public Integer getOrDefault(Object key, Integer defaultValue) {
                Integer result = super.getOrDefault(key, defaultValue);
                this.put((Long) key, result + 1);
                return result;
            }
        });
    }

    public Long getNewLongId()  throws IdGenerationException{
        return getNewLongId(Instant.now());
    }

    public Long getNewLongId(Date idTime) throws IdGenerationException {
        return getNewLongId(idTime.toInstant());
    }

    public Long getNewLongId(Instant idTime) throws IdGenerationException {
        try {
            Long ret = Long.parseLong(getNewId(idTime), 10);
            if(ret < 0L) {
                throw new IdGenerationException("ID exceeds Max Long.");
            }
            return ret;
        } catch (NumberFormatException e) {
            throw new IdGenerationException("Unable to parse id to long.", e);
        }
    }

    public String getNewId()  throws IdGenerationException{
        return getNewId(Instant.now());
    }

    public String getNewId(Date idTime) throws IdGenerationException{
        return getNewId(idTime.toInstant());
    }

    public String getNewId(Instant instant) throws IdGenerationException{

        Long epochMilli = instant.toEpochMilli();

        if(epochMilli > 9223372036854L) {
            throw new IdGenerationException("Timestamp greater than allowed value.");
        }

        // Get next increment for the timestamp
        Integer nextIncrement = history.getOrDefault(epochMilli,0);

        if(nextIncrement > 999) {
            throw new IdGenerationException("All increments used for given millisecond.");
        }

        // We need to assemble our ID.
        String epoch = String.format("%013d",epochMilli);
        String epochTrimmed = epoch.substring(epoch.length()-13);
        String increment = String.format("%03d", nextIncrement);

        return padString(epochTrimmed+increment+instanceId);
    }

    private String padString(String unpadded) {
        return "0000000000000000000".substring(unpadded.length()) + unpadded;
    }
}
