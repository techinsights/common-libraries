package io.pythagoras.common.distributedatomicincrementer;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.UpdateItemOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.model.*;
import io.pythagoras.common.distributedatomicincrementer.exceptions.InitializationException;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class IncrementProvider {

    private DynamoDB dynamoDB;
    private Table table;
    private DynamoConfig config;
    private Boolean isInitialized = false;

    public IncrementProvider(DynamoDB dynamoDB, DynamoConfig config) {
        this.dynamoDB = dynamoDB;
        this.config = config;
    }

    public IncrementProvider(String awsRegion, AWSCredentialsProvider credentials, DynamoConfig config){

        this.config = config;
        AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
                .withRegion(awsRegion)
                .withCredentials(credentials)
                .build();
        this.dynamoDB = new DynamoDB(client);
    }

    public void initialize() throws InitializationException {
        this.table = this.getOrCreateTable();
        this.validateTable();
        this.isInitialized = true;
    }

    Table getOrCreateTable() {
        CreateTableRequest request = new CreateTableRequest()
                .withTableName(config.getTableName())
                .withProvisionedThroughput(new ProvisionedThroughput().withReadCapacityUnits(5L).withWriteCapacityUnits(5L))
                .withKeySchema(new KeySchemaElement().withKeyType(KeyType.HASH).withAttributeName("counter_id"))
                .withAttributeDefinitions(
                        new AttributeDefinition().withAttributeName("counter_id").withAttributeType(ScalarAttributeType.S)
                        );
        Table table;
        try {
            table = this.dynamoDB.createTable(request);
            table.waitForActive();
        } catch (ResourceInUseException | InterruptedException e) {
            // Table already exists.  Lets set it.
            table = this.dynamoDB.getTable(this.config.getTableName());
        }
        return table;
    }

    void validateTable() throws InitializationException {
        // Validate that it exists and has a record for the given key.
        try {
            this.table.describe();
            this.updateItem(0);
        } catch (ResourceNotFoundException e) {
            throw new InitializationException("Unable to initialize Increment Provider.",e);
        }
    }

    public Integer incrementCounterAndGetNewValue() throws InitializationException {
        if(!this.isInitialized) {
            this.initialize();
        }

        // Claim the next count
        Integer cnt = extractCounterFromItem(updateItem(config.getCountQuantity()));

        // Handle the rollover.
        return reduceFromRollover(cnt);
    }

    Integer reduceFromRollover(Integer cnt) {
        return cnt % config.getRolloverQty();
    }

    Item updateItem(Integer incrementAmmount) throws ResourceNotFoundException {


        Map<String, String> expressionAttributeNames = new HashMap<>();
        expressionAttributeNames.put("#C", "cnt");

        Map<String,Object> expressionAttributeValues = new HashMap<>();
        expressionAttributeValues.put(":val", incrementAmmount);

        UpdateItemOutcome outcome = table.updateItem(new UpdateItemSpec()
        .withPrimaryKey(config.getPrimaryKeyName(),config.getPrimaryKeyValue())
        .withNameMap(expressionAttributeNames)
                .withValueMap(expressionAttributeValues)
                .withUpdateExpression("add #C :val")
                .withReturnValues(ReturnValue.UPDATED_NEW));

        return outcome.getItem();
    }

    Integer extractCounterFromItem(Item item) {
        Object cnt = item.get("cnt");
        if(cnt instanceof Integer) {
            return (Integer) cnt;
        }
        return ((BigDecimal) cnt).intValue();
    }
}
