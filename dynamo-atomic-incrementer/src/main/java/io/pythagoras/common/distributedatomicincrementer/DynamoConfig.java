package io.pythagoras.common.distributedatomicincrementer;

public class DynamoConfig {

    private String tableName;
    private String primaryKeyName;
    private String primaryKeyValue;
    private Integer countQuantity = 1;
    private Integer rolloverQty = Integer.MAX_VALUE;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getPrimaryKeyName() {
        return primaryKeyName;
    }

    public void setPrimaryKeyName(String primaryKeyName) {
        this.primaryKeyName = primaryKeyName;
    }

    public String getPrimaryKeyValue() {
        return primaryKeyValue;
    }

    public void setPrimaryKeyValue(String primaryKeyValue) {
        this.primaryKeyValue = primaryKeyValue;
    }

    public Integer getCountQuantity() {
        return countQuantity;
    }

    public void setCountQuantity(Integer countQuantity) {
        this.countQuantity = countQuantity;
    }

    public Integer getRolloverQty() {
        return rolloverQty;
    }

    public void setRolloverQty(Integer rolloverQty) {
        this.rolloverQty = rolloverQty;
    }
}
