package io.pythagoras.common.distributedatomicincrementer;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import io.pythagoras.common.distributedatomicincrementer.exceptions.InitializationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class IncrementProviderUnitTests {

    private IncrementProvider incrementProvider;
    private DynamoConfig config = new DynamoConfig();

    @BeforeEach
    public void setupEach() throws InitializationException {

        config.setCountQuantity(1);
        config.setPrimaryKeyName("counter_id");
        config.setPrimaryKeyValue("BusHistory_InstanceId");
        config.setRolloverQty(1000);
        config.setTableName("atomic_counters");

        DynamoDB dynamoDB = Mockito.mock(DynamoDB.class);

        incrementProvider = new IncrementProvider(dynamoDB,config);
    }

    @Test
    public void reduceFromRollover_1() {
        Assertions.assertEquals(0,incrementProvider.reduceFromRollover(0).intValue());
        Assertions.assertEquals(0,incrementProvider.reduceFromRollover(1000).intValue());
        Assertions.assertEquals(999,incrementProvider.reduceFromRollover(999).intValue());
        Assertions.assertEquals(999,incrementProvider.reduceFromRollover(1999).intValue());
        Assertions.assertEquals(0,incrementProvider.reduceFromRollover(2000).intValue());
    }

    @Test
    public void extractCounterFromItem_1() {

        Item item = new Item();
        item.withNumber("cnt",3);
        item.withString(config.getPrimaryKeyName(), config.getPrimaryKeyValue());

        Assertions.assertEquals(3,incrementProvider.extractCounterFromItem(item).intValue());
    }
}
