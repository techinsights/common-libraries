package io.pythagoras.common.distributedatomicincrementer;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import org.junit.jupiter.api.Assertions;

public class IncrementProviderIntegrationTests {

    IncrementProvider incrementProvider;

    //@BeforeEach
    public void setupAll() throws Exception {
        DynamoDB dynamoDB = new DynamoDB(AmazonDynamoDBClientBuilder.standard()
                .withRegion("")
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(
                        "",
                        ""
                )))
                .build());

        DynamoConfig config = new DynamoConfig();
        config.setCountQuantity(1);
        config.setPrimaryKeyName("counter_id");
        config.setPrimaryKeyValue("Test_InstanceId");
        config.setRolloverQty(1000);
        config.setTableName("atomic_counters");

        incrementProvider = new IncrementProvider(dynamoDB,config);
        incrementProvider.initialize();
    }

    //@Test
    public void updateItem() {
        Item item = incrementProvider.updateItem(1);
        Integer cnt = incrementProvider.extractCounterFromItem(item);
        Assertions.assertEquals(2,cnt.intValue());
    }




}
