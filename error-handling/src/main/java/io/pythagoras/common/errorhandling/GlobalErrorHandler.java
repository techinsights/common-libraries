package io.pythagoras.common.errorhandling;

import com.google.gson.Gson;
import io.pythagoras.common.errorhandling.exceptions.BaseException;
import io.pythagoras.common.errorhandling.exceptions.HTTPException;
import io.pythagoras.common.loggercommon.LogObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ControllerAdvice
@RestControllerAdvice
public class GlobalErrorHandler {
    private static final Logger logger = LoggerFactory.getLogger(GlobalErrorHandler.class);

    GlobalErrorHandler() {
        // Load up the defaults for the status mapper.
        StatusExceptionMap.registerMapping(Exception.class, HttpStatus.INTERNAL_SERVER_ERROR);
        StatusExceptionMap.registerMapping(RuntimeException.class, HttpStatus.INTERNAL_SERVER_ERROR);
        StatusExceptionMap.registerMapping(BaseException.class, HttpStatus.INTERNAL_SERVER_ERROR);
        StatusExceptionMap.registerMapping(MethodArgumentNotValidException.class, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<?> handleException(Exception e) {

        // Lets resolve using the exception map
        HttpStatus status = null;
        Class<? extends Exception> exClass = e.getClass();

        do {
            status = StatusExceptionMap.getStatusForException(exClass);
            if (status == null) {
                exClass = (Class<? extends Exception>) exClass.getSuperclass();
            }
        } while (status == null && exClass != null);

        if (status == null) {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return handleException(e, status.value());
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> handleFormValidationException(MethodArgumentNotValidException e) {
        if (e.getBindingResult() != null && e.getBindingResult().hasErrors() && e.getBindingResult().getAllErrors() != null) {
            StringBuilder sb = new StringBuilder();
            List<ObjectError> errors = e.getBindingResult().getAllErrors();
            int count = errors.size();
            ObjectError error;
            for (int x = 0; x < count; x++) {
                error = errors.get(x);
                if (error instanceof FieldError) {
                    sb.append(((FieldError) error).getField()).append(" : ").append(error.getDefaultMessage());
                } else {
                    sb.append(error.getDefaultMessage());
                }
                if (x < count - 1) {
                    sb.append(", ");
                }
            }
            return handleException(e, sb.toString(), HttpStatus.BAD_REQUEST.value());
        } else {
            return handleException(e, HttpStatus.BAD_REQUEST.value());
        }
    }

    @ExceptionHandler(value = HTTPException.class)
    public ResponseEntity<?> handleHTTPException(HTTPException e) {
        return handleException(e, e.getStatusCode());
    }

    private ResponseEntity<?> handleException(Exception e, int statusCode) {
        return handleException(e, null, statusCode);
    }

    private ResponseEntity<?> handleException(Exception e, String customMessage, int statusCode) {

        Gson gson = new Gson();
        String errorMessage = e.getMessage() != null ? e.getMessage() : "No message in exception.";

        Map<String, Object> obj = new HashMap<>();
        obj.put("message", customMessage != null ? customMessage : errorMessage);
        obj.put("statusCode", statusCode);
        String response = gson.toJson(obj);

        logError(e, statusCode, errorMessage);

        return ResponseEntity.status(statusCode).body(response);
    }

    private void logError(Exception e, int statusCode, String errorMessage ) {
        LogObject logObject = new LogObject();
        logObject.statusCode = statusCode;
        logObject.exception = e;

        logger.error(errorMessage, logObject, e);
    }
}

