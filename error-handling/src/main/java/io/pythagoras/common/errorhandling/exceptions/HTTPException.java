package io.pythagoras.common.errorhandling.exceptions;

import org.springframework.http.HttpStatus;

import java.util.HashMap;

public class HTTPException extends BaseException {

    private HttpStatus statusCode;

    private static HashMap<Integer, String> statusCodeMap = new HashMap<Integer, String>();

    public HTTPException(String message, int statusCode) {
        super(message);
        this.setStatusCode(statusCode);
    }

    public HTTPException(String message, Throwable cause, int statusCode) {
        super(message, cause);
        this.setStatusCode(statusCode);
    }

    public HTTPException(Throwable cause, int statusCode) {
        super(cause);
        this.setStatusCode(statusCode);
    }

    public HTTPException(int statusCode) {
        this.setStatusCode(statusCode);
    }

    public int getStatusCode() {
        return statusCode.value();
    }

    public String getStatusMessage() {
        return this.statusCode.toString();
    }


    private void setStatusCode(int code) throws IllegalArgumentException {
        HttpStatus status = HttpStatus.valueOf(code);
        if (!(status.is4xxClientError() || status.is5xxServerError())) {
            throw new IllegalArgumentException("No reason to throw an HTTPException for a status code of " + code);
        }
        this.statusCode = status;
    }
}
