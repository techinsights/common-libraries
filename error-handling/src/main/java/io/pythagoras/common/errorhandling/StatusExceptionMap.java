package io.pythagoras.common.errorhandling;

import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

public class StatusExceptionMap {

    private static Map<Class<? extends Exception>, HttpStatus> statusMap = new HashMap<>();

    public static <T extends Exception> void registerMapping(Class<T> klass, HttpStatus status) {
        statusMap.put(klass, status);
    }

    public static <T extends Exception> HttpStatus getStatusForException(Class<T> exception) {
        return statusMap.getOrDefault(exception, null);
    }
}
