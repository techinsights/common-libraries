package io.pythagoras.common.errorhandling;

import com.google.gson.Gson;
import io.pythagoras.common.errorhandling.exceptions.HTTPException;
import io.pythagoras.common.errorhandling.exceptions.BaseException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
public class GlobalErrorHandlerTest {

    private static final Logger logger = LoggerFactory.getLogger(GlobalErrorHandlerTest.class);

    private GlobalErrorHandler geh;

    @BeforeEach
    public void setup() {
        geh = new GlobalErrorHandler();
    }

    @Test
    public void ShouldHandleException() {
        String message = "A message";
        validateResponse(geh.handleException(new Exception(message)), message, 500);
    }

    @Test
    public void ShouldHandleBaseException() {
        String message = "A message";
        validateResponse(geh.handleException(new BaseException(message)), message, 500);
    }

    @Test
    public void ShouldHandleHTTPException4xx() {
        String message = "A message";
        validateResponse(geh.handleHTTPException(new HTTPException(message, 400)), message, 400);
    }

    @Test
    public void ShouldHandleHTTPException5xx() {
        String message = "A message";
        validateResponse(geh.handleHTTPException(new HTTPException(message, 500)), message, 500);
    }

    @Test
    public void Temp() {
        logger.error("TEST1", new testClass(), new RuntimeException("EX1"));
    }

    @Test
    public void ShouldHandleUnknownException() {
        String message = "A message";
        validateResponse(geh.handleException(new MyException(message)), message, 500);
    }

    @Test
    public void ShouldHandleMappedException() {
        StatusExceptionMap.registerMapping(MyException.class,HttpStatus.valueOf(400));
        String message = "A message";
        validateResponse(geh.handleException(new MyException(message)), message, 400);
    }


    private void validateResponse(ResponseEntity<?> response, String message, Integer statusCode) {
        Map<String, Object> obj = new HashMap<>();
        obj.put("message", message);
        obj.put("statusCode", statusCode);
        Gson gson = new Gson();
        assertEquals(gson.toJson(obj), response.getBody());
        assertEquals(HttpStatus.valueOf(statusCode), response.getStatusCode());
    }

    private class testClass {
        @Override
        public String toString() {
            return "THIS IS STRING";
        }
    }


    class MyException extends RuntimeException {
        public MyException(String message) {
            super(message);
        }
    }
}
