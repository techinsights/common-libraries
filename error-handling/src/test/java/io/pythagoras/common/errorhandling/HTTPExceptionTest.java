package io.pythagoras.common.errorhandling;

import io.pythagoras.common.errorhandling.exceptions.HTTPException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
public class HTTPExceptionTest {

    @Test
    public void validStatusCode400() {
        HTTPException ex = new HTTPException(400);
        assertThat(ex).isInstanceOf(HTTPException.class);
    }

    @Test
    public void validStatusCode500() {
        HTTPException ex = new HTTPException(500);
        assertThat(ex).isInstanceOf(HTTPException.class);
    }

    @Test
    public void invalidStatusCode200() {
        assertThrows(IllegalArgumentException.class, () -> new HTTPException(200));
    }

    @Test
    public void invalidStatusCode300() {
        assertThrows(IllegalArgumentException.class, () -> new HTTPException(300));
    }

    @Test
    public void invalidStatusCode() {
        assertThrows(IllegalArgumentException.class, () -> new HTTPException(10373));
    }

    @Test
    public void getsStatusCode() {
        HTTPException ex = new HTTPException(404);
        assertEquals(HttpStatus.NOT_FOUND.value(), ex.getStatusCode());
        ex = new HTTPException(503);
        assertEquals(HttpStatus.SERVICE_UNAVAILABLE.value(), ex.getStatusCode());
    }

    @Test
    public void getsStatusMessage() {
        HTTPException ex = new HTTPException(404);
        assertEquals(HttpStatus.NOT_FOUND.toString(), ex.getStatusMessage());
        ex = new HTTPException(503);
        assertEquals(HttpStatus.SERVICE_UNAVAILABLE.toString(), ex.getStatusMessage());
    }
}