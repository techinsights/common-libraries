package io.pythagoras.common.grayloglogger;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UniqueInstanceID {
    private static final String INSTANCE_ID = UUID.randomUUID().toString();

    public String getInstanceId() {
        return INSTANCE_ID;
    }

    @Override
    public String toString() {
        return getInstanceId();
    }
}
