package io.pythagoras.common.grayloglogger.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("pythagoras.graylog-logger")
public class GraylogProperties {

    private String host = "localhost";

    private Integer port = 12201;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }


    /** Additional properties added for the custom JSON File logging **/

    private boolean enableLoggingToFile = false;
    private String logFileMaxSize = "50MB";
    private String logFileName = "logFile.log";

    private boolean enableJsonInConsole = true;


    public boolean getEnableLoggingToFile() { return this.enableLoggingToFile; }
    public void setEnableLoggingToFile(boolean enableLoggingToFile) { this.enableLoggingToFile = enableLoggingToFile; }

    public String getLogFileMaxSize() { return logFileMaxSize; }
    public void setLogFileMaxSize(String logFileMaxSize) { this.logFileMaxSize = logFileMaxSize; }

    public String getLogFileName() { return this.logFileName; }
    public void setLogFileName(String logFileName) { this.logFileName = logFileName; }

    public boolean isEnableJsonInConsole() { return enableJsonInConsole; }
    public void setEnableJsonInConsole(boolean enableJsonInConsole) { this.enableJsonInConsole = enableJsonInConsole; }
}
