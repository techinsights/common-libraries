package io.pythagoras.common.grayloglogger;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GraylogLoggerConfig {

    @Autowired
    CustomJsonFileAppender customFileAppender;

    @Autowired
    ConsoleAppenderWrapper consoleAppenderWrapper;


    @Bean
    Logger logger() {
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

        // Override the console appender
        Object ca = root.getAppender("CONSOLE");
        if (ca instanceof ConsoleAppender) {
            consoleAppenderWrapper.setName("Console Appender");
            consoleAppenderWrapper.setContext(context);
            consoleAppenderWrapper.setConsoleAppender((ConsoleAppender<ILoggingEvent>) ca);
            consoleAppenderWrapper.start();
            root.detachAppender("CONSOLE");
            root.addAppender(consoleAppenderWrapper);
        }

        // Add the custom json file appender (e.g., to be tailed: e.g., by LogStash, DataDog, etc.)
        customFileAppender.setName("JSON File Appender");
        customFileAppender.setContext(context);
        customFileAppender.start();
        root.addAppender(customFileAppender);

        return null;
    }

}
