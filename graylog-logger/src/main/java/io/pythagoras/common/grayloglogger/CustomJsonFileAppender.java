package io.pythagoras.common.grayloglogger;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Context;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;
import ch.qos.logback.core.util.FileSize;
import io.pythagoras.common.grayloglogger.config.GraylogProperties;
import io.pythagoras.common.loggercommon.LogObject;
import net.logstash.logback.encoder.LogstashEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class CustomJsonFileAppender extends RollingFileAppender<ILoggingEvent> {

    private GraylogProperties properties;

    private String appName;

    private UniqueInstanceID instanceID;

    @Autowired
    public void setProperties(GraylogProperties properties) {
        this.properties = properties;
    }

    @Autowired
    public void setInstanceID(UniqueInstanceID instanceID) { this.instanceID = instanceID; }

    @Value("${spring.application.name:NAME_NOT_FOUND}")
    public void setAppName(String appName) {
        this.appName = appName;
    }

    @Override
    public void start() {
        if (this.properties.getEnableLoggingToFile()) {
            buildWithContext(this.context);
            super.start();
        }
    }

    private void buildWithContext(Context context) {
        this.setContext(context);
        this.setName("TICustomFileAppender");
        this.setFile(properties.getLogFileName());
        this.setImmediateFlush(true);
        this.setAppend(false);

        // Logstash JSON Formatting
        LogstashEncoder encoder = new LogstashEncoder();
        encoder.setContext(context);
        encoder.setIncludeContext(false);
        encoder.setIncludeMdc(true);
        encoder.setCustomFields("{\"app_name\":\"" + appName + "\",\"instance_id\":\"" + instanceID.getInstanceId() + "\"}");
        encoder.start();
        this.setEncoder(encoder);

        // Set rolling file policy
        final int maxHistory = 2;
        final long totalCapSize = FileSize.valueOf(properties.getLogFileMaxSize()).getSize();
        final long maxIndFileSize = totalCapSize / (long) maxHistory;
        TimeBasedRollingPolicy<ILoggingEvent> policy = new TimeBasedRollingPolicy<>();
        policy.setContext(context);
        policy.setMaxHistory(maxHistory);
        policy.setTotalSizeCap(new FileSize(totalCapSize));
        policy.setFileNamePattern(properties.getLogFileName() + "-%d{yyyy-MM-dd-HH-mm}-%i.log.gz");
        policy.setParent(this);
        policy.start();

        SizeAndTimeBasedFNATP<ILoggingEvent> innerPolicy = new SizeAndTimeBasedFNATP<>();
        innerPolicy.setContext(context);
        innerPolicy.setMaxFileSize(new FileSize(maxIndFileSize));
        innerPolicy.setTimeBasedRollingPolicy(policy);
        innerPolicy.start();

        policy.setTimeBasedFileNamingAndTriggeringPolicy(innerPolicy);
        policy.start();

        this.setRollingPolicy(policy);
    }


    @Override
    protected void append(ILoggingEvent eventObject) {
        // For compatability with the GraylogAppender (and its custom "LogObject"):
        boolean logObjectAdded = false;
        Object[] args = eventObject.getArgumentArray();
        if (hasArguments(args)) {
            Object obj = eventObject.getArgumentArray()[0];
            if (obj instanceof LogObject) {
                logObjectAdded = true;
                eventObject.getMDCPropertyMap().put("StatusCode", Integer.toString(((LogObject) obj).statusCode));
            }
        }

        super.append(eventObject);

        if (logObjectAdded) {
            eventObject.getMDCPropertyMap().remove("StatusCode");
        }
    }

    private boolean hasArguments(Object[] args) {
        return (args != null) && (args.length > 0);
    }
}
