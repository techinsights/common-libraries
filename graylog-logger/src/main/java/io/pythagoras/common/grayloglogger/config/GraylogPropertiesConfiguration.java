package io.pythagoras.common.grayloglogger.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(GraylogProperties.class)
public class GraylogPropertiesConfiguration {
}
