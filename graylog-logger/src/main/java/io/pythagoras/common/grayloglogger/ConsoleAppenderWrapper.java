package io.pythagoras.common.grayloglogger;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.classic.spi.ThrowableProxy;
import ch.qos.logback.core.AppenderBase;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.Context;
import io.pythagoras.common.grayloglogger.config.GraylogProperties;
import io.pythagoras.common.loggercommon.LogObject;
import net.logstash.logback.encoder.LogstashEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ConsoleAppenderWrapper extends AppenderBase<ILoggingEvent> {

    private ConsoleAppender<ILoggingEvent> consoleAppender;

    private GraylogProperties properties;

    private String appName;

    private UniqueInstanceID instanceID;

    @Autowired
    public void setProperties(GraylogProperties properties) {
        this.properties = properties;
    }

    @Autowired
    public void setInstanceID(UniqueInstanceID instanceID) { this.instanceID = instanceID; }

    @Value("${spring.application.name:NAME_NOT_FOUND}")
    public void setAppName(String appName) {
        this.appName = appName;
    }

    public void setConsoleAppender(ConsoleAppender<ILoggingEvent> consoleAppender) {
        this.consoleAppender = consoleAppender;
    }

    @Override
    public void start() {
        if (this.properties.isEnableJsonInConsole()) {
            configureJsonFormatting(this.context);
        }
        super.start();
    }

    private void configureJsonFormatting(Context context) {
        // Logstash JSON Formatting
        LogstashEncoder encoder = new LogstashEncoder();
        encoder.setContext(context);
        encoder.setIncludeContext(false);
        encoder.setIncludeMdc(true);
        encoder.setCustomFields("{\"app_name\":\"" + appName + "\",\"instance_id\":\"" + instanceID.getInstanceId() + "\"}");
        encoder.start();

        consoleAppender.setContext(context);
        consoleAppender.setEncoder(encoder);
    }

    @Override
    protected void append(ILoggingEvent iLoggingEvent) {

        // For compatability with the custom "LogObject":
        if (iLoggingEvent instanceof ch.qos.logback.classic.spi.LoggingEvent) {

            Object[] args = iLoggingEvent.getArgumentArray();
            boolean logObjectAdded = false;
            if (args != null && args.length > 0) {
                Object obj = iLoggingEvent.getArgumentArray()[0];
                if (obj instanceof LogObject) {
                    // We have a logged error.  Lets unwrap it.
                    logObjectAdded = true;
                    iLoggingEvent.getMDCPropertyMap().put("StatusCode", Integer.toString(((LogObject) obj).statusCode));

                    if (iLoggingEvent.getThrowableProxy() == null) {
                        ((LoggingEvent) iLoggingEvent).setThrowableProxy(new ThrowableProxy(((LogObject) obj).exception));
                    }
                }
            }

            this.consoleAppender.doAppend(iLoggingEvent);

            if (logObjectAdded) {
                iLoggingEvent.getMDCPropertyMap().remove("StatusCode");
            }
        }
    }
}
