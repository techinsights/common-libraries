package io.pythagoras.common.swaggerui;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("pythagoras.swagger")
public class SwaggerProperties {

    class OAuth2 {

        private String clientId;

        private String clientSecret;

        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }

        public String getClientSecret() {
            return clientSecret;
        }

        public void setClientSecret(String clientSecret) {
            this.clientSecret = clientSecret;
        }
    }

    private OAuth2 oauth = new OAuth2();

    private String authorizationTokenEndpoint;

    private String name;

    private String description;

    public OAuth2 getOauth() {
        return oauth;
    }

    public String getAuthorizationTokenEndpoint() {
        return authorizationTokenEndpoint;
    }

    public void setAuthorizationTokenEndpoint(String authorizationTokenEndpoint) {
        this.authorizationTokenEndpoint = authorizationTokenEndpoint;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
