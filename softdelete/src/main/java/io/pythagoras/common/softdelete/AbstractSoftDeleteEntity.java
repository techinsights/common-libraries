package io.pythagoras.common.softdelete;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
public abstract class AbstractSoftDeleteEntity<ID extends Serializable> implements SoftDeleteEntity<ID> {

    @Column(name = "is_deleted", columnDefinition = "Boolean default false")
    private Boolean isDeleted = false;

    public abstract ID getId();

}
