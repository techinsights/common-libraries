package io.pythagoras.common.softdelete;


import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import java.io.Serializable;

@NoRepositoryBean
public interface SoftDeleteableCrudRepository<ID extends Serializable, T extends SoftDeleteEntity<ID>> extends CrudRepository<T, ID> {

    @Query("update #{#entityName} e set e.isDeleted=true where e.id = ?1")
    @Transactional
    @Modifying
    void delete(ID id);

    @Override
    @Transactional
    default void delete(T entity) {
        delete(entity.getId());
    }

    @Transactional
    default void delete(Iterable<? extends T> entities) {
        entities.forEach(entitiy -> delete(entitiy.getId()));
    }

    @Override
    @Query("update #{#entityName} e set e.isDeleted=true")
    @Transactional
    @Modifying
    void deleteAll();
}
