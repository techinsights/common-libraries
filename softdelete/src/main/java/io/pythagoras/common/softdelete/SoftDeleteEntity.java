package io.pythagoras.common.softdelete;

import jakarta.persistence.MappedSuperclass;
import org.hibernate.annotations.Where;
import java.io.Serializable;

/**
 * This interface can be used to achieve soft-delete functionality for an entity.  The preferred way is to
 * extend the AbstractSoftDeleteEntity class as it already implements this interface.  If that is not
 * feasible, then implement this interface on an entity.  A property of type boolean, and with
 * property name "isDeleted" and with db field mapping of "is_deleted" with a default value of "false" is also necessary.
 * @param <ID> extends Serializable.   The type of the ID on the entity. Ex.  String, Integer, UUID, etc.
 */
@Where(clause = "is_deleted=false")
@MappedSuperclass
public interface SoftDeleteEntity<ID extends Serializable> extends Serializable{

    ID getId();


}
